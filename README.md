# README #

Test docker build pipeline. A simple flask app that creates a local website on port 5000. On commit, builds a docker image and the pushes to Docker Hub.

Run the below to pull and run the docker image created:  
`docker run -p 5000:5000 andrewolau/test_pipeline`

Then open your browser and go to <http://0.0.0.0:5000/>