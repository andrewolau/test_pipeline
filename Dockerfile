FROM python:3.10.0a7-slim-buster

# WORKDIR /home/andrew/Desktop/docker/flask-heads

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY app.py app.py

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
